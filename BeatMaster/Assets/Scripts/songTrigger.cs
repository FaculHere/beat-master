﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class songTrigger : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        // Calls upon playAudio function after a delay of 2 senconds
        Invoke("playAudio", 2);
        
        // Calls upon endSong function after a delay of 2 seconds and repeats to call the funtion once everysecond
        InvokeRepeating("endSong", 2, 1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void playAudio()
    {
        // Starts playing the audio file which is attached to the same gameobject as this script
        GetComponent<AudioSource>().Play();
    }

    void endSong()
    {
        // Checks if song has finished playing
        if (!GetComponent<AudioSource>().isPlaying)
        {
            // Transisitons from the level screen to the level highscore scene
            SceneManager.LoadScene(destroyOutOfBounds.nextScene);
        }
    }


}
