﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class highscoreCollection : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        destroyOutOfBounds.currentScene = SceneManager.GetActiveScene().buildIndex;

        // load existing scores
        for (int i = 0; i < highscores1.Length; ++i)
        {
            highscores1[i] = PlayerPrefs.GetFloat("highscores1"+i,0);
        }

        for (int i = 0; i < highscores2.Length; ++i)
        {
            highscores2[i] = PlayerPrefs.GetFloat("highscores2" + i, 0);
        }

        for (int i = 0; i < highscores3.Length; ++i)
        {
            highscores3[i] = PlayerPrefs.GetFloat("highscores3" + i, 0);
        }

        // Compare current score and put it in one of the arrays if applicable
        sortHighScores();

        // Save updated scores
        for (int i = 0; i < highscores1.Length; ++i)
        {
            PlayerPrefs.SetFloat("highscores1" + i, highscores1[i]);
        }

        for (int i = 0; i < highscores2.Length; ++i)
        {
            PlayerPrefs.SetFloat("highscores2" + i, highscores2[i]);
        }

        for (int i = 0; i < highscores3.Length; ++i)
        {
            PlayerPrefs.SetFloat("highscores3" + i, highscores3[i]);
        }

        // Save to disk
        PlayerPrefs.Save();
    }

    private static float[] highscores1 = new float[5];
    private static float[] highscores2 = new float[5];
    private static float[] highscores3 = new float[5];
    public Text userScore;
    public Text pos1;
    public Text pos2;
    public Text pos3;
    public Text pos4;
    public Text pos5;
    private float temp = iconGreenHit.currentScore;
    private float secondTemp;
    private int counter;

    // Update is called once per frame
    void Update()
    {
        
        userScore.text = "" + iconGreenHit.currentScore;

        // Checks which scene is currently loaded and retreives the appropriate highscore values for the song the user has just completed
        if (destroyOutOfBounds.currentScene == 3)
        {
            // Sends the relevant scores to the text gameobjects within the scene
            pos1.text = "" + highscores1[0];
            pos2.text = "" + highscores1[1];
            pos3.text = "" + highscores1[2];
            pos4.text = "" + highscores1[3];
            pos5.text = "" + highscores1[4];
        }
        else if (destroyOutOfBounds.currentScene == 5)
        {
            pos1.text = "" + highscores2[0];
            pos2.text = "" + highscores2[1];
            pos3.text = "" + highscores2[2];
            pos4.text = "" + highscores2[3];
            pos5.text = "" + highscores2[4];
        }
        else if (destroyOutOfBounds.currentScene == 7)
        {
            pos1.text = "" + highscores3[0];
            pos2.text = "" + highscores3[1];
            pos3.text = "" + highscores3[2];
            pos4.text = "" + highscores3[3];
            pos5.text = "" + highscores3[4];
        }
    }

    void sortHighScores()
    {
        // Checks which scene is currently loaded and retreives the appropriate highscore values for the song the user has just completed
        if (destroyOutOfBounds.currentScene == 3)
        {
            // Repeats the code which within the loop for four cycles each cycle gradually sorts the list of values until the values are in decending order
            for (counter = 0; counter < 5; counter++)
            {
                // Checks if the temp value is greater than the current value being stored in the current postion of the highscores1 array
                if (temp > highscores1[counter])
                {
                    // Sets the secondTemp value to the of the current value being stored in the current postion of the highscores1 array
                    secondTemp = highscores1[counter];

                    // Changes the value being stored in the current postion of the highscores1 array to the value being store in the temp variable
                    highscores1[counter] = temp;

                    // Changes the temp value to the secondTemp value
                    temp = secondTemp;
                }
            }
        }else if(destroyOutOfBounds.currentScene == 5)
        {
            for (counter = 0; counter < 5; counter++)
            {
                if (temp > highscores2[counter])
                {
                    secondTemp = highscores2[counter];
                    highscores2[counter] = temp;
                    temp = secondTemp;
                }
            }
        }else if(destroyOutOfBounds.currentScene == 7)
        {
            for (counter = 0; counter < 5; counter++)
            {
                if (temp > highscores3[counter])
                {
                    secondTemp = highscores3[counter];
                    highscores3[counter] = temp;
                    temp = secondTemp;
                }
            }
        }
        
    }
}
