﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class songMenu : MonoBehaviour
{
    // Sets the interval value to 0.5 which is equal to half a second
    public float interval = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        // Calls upon the menuScroll function with no delay and repeats at the interval set in the gameobject properties
        // This was implemented to solve an issue regaurding the menu scroll skipping options within the menu
        InvokeRepeating("menuScroll", 0, interval);
    }

    public float verticalInput;
    private int playerChoice;
    public GameObject choiceIndicator;

    // Update is called once per frame
    void Update()
    {
        // Checks if playerChoice value exceeds the limits the choices available and if so reverts the value back to the the limit
        if (playerChoice > 2)
        {
            playerChoice = 2;
        }else if(playerChoice < 0)
        {
            playerChoice = 0;
        }

        // Checks if 'B' button is pressed
        if(Input.GetButtonDown("xboxB"))
        {
            // Changes scene back to title screen
            SceneManager.LoadScene(0);
            
        }

        // Checks if 'A' button is pressed whilst the first option is selected
        if (Input.GetButtonDown("xboxA") && playerChoice == 0)
        {
            // Changes scene to the appropriate scene for the song described within the menu
            SceneManager.LoadScene(2);
        }
        // Checks if player choice value is 0
        else if (playerChoice == 0)
        {
            // Moves the flashing underscore to the appropriate position to indicate to the user that option 1 (playerChoice = 0) is currently selected in the menu
            choiceIndicator.transform.position = new Vector3(4, 1, 0);
        }

        if(Input.GetButtonDown("xboxA") && playerChoice == 1)
        {
            SceneManager.LoadScene(4);
        }else if(playerChoice == 1)
        {
            choiceIndicator.transform.position = new Vector3(3, -1, 0);
        }

        if(Input.GetButtonDown("xboxA") && playerChoice == 2)
        {
            SceneManager.LoadScene(6);
        }else if(playerChoice == 2)
        {
            choiceIndicator.transform.position = new Vector3(5, -3, 0);
        }

    }

    void menuScroll()
    {
        //Checks if joystick is pushed up/down and increases/deceases the playerChoice value accordingly
        verticalInput = Input.GetAxis("Vertical");

        if (verticalInput > 0)
        {
            playerChoice = playerChoice - 1;
        }
        else if (verticalInput < 0)
        {
            playerChoice = playerChoice + 1;
        }
    }

}
