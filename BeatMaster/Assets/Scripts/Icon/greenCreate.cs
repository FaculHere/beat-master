﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class greenCreate : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public GameObject green;

    // Update is called once per frame
    void Update()
    {
        // Disables the destroyOutOfBounds script attached to the same gameobject

        // Checks if the 'A' button is pressed
        green.gameObject.GetComponent<destroyOutOfBounds>().enabled = false;
        if (Input.GetButtonDown("xboxA"))
        {
            // Creates a clone of the of the gameobject and gernates the new gameobject a unique identity
            Instantiate(green, transform.position, Quaternion.identity);
        }
    }
}
