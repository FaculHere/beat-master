﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class redCreate : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public GameObject red;

    // Update is called once per frame
    void Update()
    {
        // Disables the destroyOutOfBounds script attached to the same gameobject
        red.gameObject.GetComponent<destroyOutOfBounds>().enabled = false;

        // Checks if the 'B' button is pressed
        if (Input.GetButtonDown("xboxB"))
        {
            // Creates a clone of the of the gameobject and gernates the new gameobject a unique identity
            Instantiate(red, transform.position, Quaternion.identity);
        }
    }
}
