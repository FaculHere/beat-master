﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blueCreate : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public GameObject blue;

    // Update is called once per frame
    void Update()
    {
        // Disables the destroyOutOfBounds script attached to the same gameobject
        blue.gameObject.GetComponent<destroyOutOfBounds>().enabled = false;

        // Checks if the 'X' button is pressed
        if (Input.GetButtonDown("xboxX"))
        {
            // Creates a clone of the of the gameobject and gernates the new gameobject a unique identity
            Instantiate(blue, transform.position, Quaternion.identity);
        }
    }
}
