﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class iconMovement : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    // Sets speed value to 20
    public float speed = 20.0f;

    // Update is called once per frame
    void Update()
    {
        // Tells the game object to move up the screen at the speed determined by the speed variable
        transform.Translate(Vector3.up * Time.deltaTime * speed);
    }
}
