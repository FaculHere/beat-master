﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class iconYellowHit : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    private float currentPos = 0f;
    private float accuracy = 0f;
    private float upperLimit = 3.5f;
    private float lowerLimit = 2.5f;
    private float newScore = 0f;
    // Update is called once per frame
    void Update()
    {
        // Fetches the gameobjects current position on the y axis
        currentPos = transform.position.y;

        // Checks if the 'Y' button is pressed whilst the gameobject is positioned on the y axis within both the upper and lower limits
        if (Input.GetButtonDown("xboxY") && currentPos < upperLimit && currentPos > lowerLimit)
        {
            // Increases the hitStreak value by 1
            iconGreenHit.hitStreak = iconGreenHit.hitStreak + 1;

            // Resets tha faultStreak value back to 0
            destroyOutOfBounds.faultStreak = 0;

            // Calls upon the calculateMultiplier function
            calculateMultiplier();

            // Calls upon the calculateScore function
            calculateScore();

            // Destroys the gameobject the script is attached to
            Destroy(gameObject);
        }

    }

    void calculateMultiplier()
    {
        // Checks if histreak value is less than 5
        if (iconGreenHit.hitStreak < 5)
        {
            // sets multiplier value to 1
            iconGreenHit.multiplier = 1;
        }
        else if (iconGreenHit.hitStreak < 9)
        {
            iconGreenHit.multiplier = 2;
        }
        else if (iconGreenHit.hitStreak < 13)
        {
            iconGreenHit.multiplier = 4;
        }
        else if (iconGreenHit.hitStreak < 17)
        {
            iconGreenHit.multiplier = 8;
        }
        else
        {
            iconGreenHit.multiplier = 16;
        }
    }

    void calculateScore()
    {
        // Checks if currentPos value is less than 3
        if (currentPos > 3)
        {
            // Sets accuracy value to 1 minus the result of currentPos minus 3
            accuracy = 1 - (currentPos - 3);
        }
        // Checks if currentPos is larger than 3
        else if (currentPos < 3)
        {
            // Sets accuracy value to 1 minus the result of 3 minus currentPos
            accuracy = 1 - (3 - currentPos);
        }

        // Sets new score to the value resulting from multiplying the values from both the accuracy and amulitplier varibales together and then also multiplying that value by 1000
        newScore = 1000 * accuracy * iconGreenHit.multiplier;

        // Adds the newScore value to the users currentScore value 
        iconGreenHit.currentScore = iconGreenHit.currentScore + newScore;
    }
}
