﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class destroyOutOfBounds : MonoBehaviour
{

    public static int currentScene;
    public static int nextScene;
    // Start is called before the first frame update
    void Start()
    {
        // Retrieves the index number of the scene currently loaded
        currentScene = SceneManager.GetActiveScene().buildIndex;
        // Sets the next scene value to 1 above the index of the value current scene so that the user will be sent to the appropriate highscore screen when they either complete or fail the song within this level
        nextScene = currentScene + 1;
    }

    private float upperBound = 5.5f;
    public static int faultStreak;
    // Update is called once per frame
    void Update()
    {
        // Checks if gameobject has exceeded the boundary on the y axis which is set up to be just off top of the screen
        if(transform.position.y > upperBound)
        {
            // Destroys the gameobject the script is attached to
            Destroy(gameObject);
            
            // Increases the fault screen value by 1
            faultStreak = faultStreak + 1;
        }

        // Checks if fault streak value equals 5
        if(faultStreak == 5)
        {
            // Changes scene to the appropraite highscore screen for the level selected
            SceneManager.LoadScene(nextScene);
        }
    }
}
