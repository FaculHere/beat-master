﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class yellowCreate : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public GameObject yellow;

    // Update is called once per frame
    void Update()
    {
        // Disables the destroyOutOfBounds script attached to the same gameobject
        yellow.gameObject.GetComponent<destroyOutOfBounds>().enabled = false;

        // Checks if the 'Y' button is pressed
        if (Input.GetButtonDown("xboxY"))
        {
            // Creates a clone of the of the gameobject and gernates the new gameobject a unique identity
            Instantiate(yellow, transform.position, Quaternion.identity);
        }
    }
}
