﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class indicatorBlink : MonoBehaviour
{

    public float startDelay = 1.5f;
    public float interval = 1.5f;

    // Start is called before the first frame update
    void Start()
    {
        // Blink function is performed repeatedly at a set interval
        InvokeRepeating("blink", startDelay, interval);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void blink()
    {
        // The sprite renderer component on the game object that this script is attached to will be toggled
        this.gameObject.GetComponent<SpriteRenderer>().enabled = !this.gameObject.GetComponent<SpriteRenderer>().enabled;
    }
}
