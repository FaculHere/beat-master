﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class highscoreMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public float verticalInput;
    public GameObject choiceIndicator;
    private int playerChoice = 0;

    // Update is called once per frame
    void Update()
    {
        verticalInput = Input.GetAxis("Vertical");

        // Checks if joystick is pushed up/down and increases/deceases the playerChoice value accordingly
        if (verticalInput > 0)
        {
            playerChoice = playerChoice - 1;
        }
        else if (verticalInput < 0)
        {
            playerChoice = playerChoice + 1;
        }

        // Checks if playerChoice value exceeds the limits the choices available and if so reverts the value back to the the limit
        if (playerChoice > 1)
        {
            playerChoice = 1;
        }
        else if (playerChoice < 0)
        {
            playerChoice = 0;
        }

        // Checks if player choice value is 0
        if (playerChoice == 0)
        {
            // Moves the flashing underscore to the appropriate position to indicate to the user that option 1 (playerChoice = 0) is currently selected in the menu
            choiceIndicator.transform.position = new Vector3(-2, 1, 0);
        }
        else if (playerChoice == 1)
        {
            choiceIndicator.transform.position = new Vector3(-2, -1, 0);
        }

        // Checks if 'A' button is pressed whilst player has option 0 (play option) selected and if so sends user to the next appropriate scene
        if (Input.GetButtonDown("xboxA") && playerChoice == 0)
        {
            // Changes scene back to the song menu
            SceneManager.LoadScene(1);

            // Resets users current score value back to 0
            iconGreenHit.currentScore = 0;

            // Resets users multiplier value back to 1
            iconGreenHit.multiplier = 1;

            // Resets users hitStreak value back to 0
            iconGreenHit.hitStreak = 0;

            // Resets users faultStreak value back to 0
            destroyOutOfBounds.faultStreak = 0;

        }

        // Checks if 'A' button is pressed whilst player has option 1 (quit option) selected and if so will quit out of the game
        if (Input.GetButtonDown("xboxA") && playerChoice == 1)
        {
            // Changes scene back to the title screen
            SceneManager.LoadScene(0);
            
        }

    }
}
